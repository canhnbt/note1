import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Bai Tap'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? usernameValue;
  String? passssValue;
  // String usernameController = '';
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passssController = TextEditingController();
  int _counter = 0;
  // @override
  // Void initState() {
  //   super.initState();
  // }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    print("$usernameValue");
    print("$passssValue");
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: usernameController,
              // onChanged: (String value) {
              //   setState(() {
              //     usernameValue = value;
              //   });
              // },
              // onSubmitted: (value) {
              //   print('tôi đã đc submit');
              // },
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  labelText: 'User name',
                  labelStyle: TextStyle(
                    color: Colors.blue,
                  ),
                  border: OutlineInputBorder()),
            ),
            TextField(
              controller: passssController,
              // onSubmitted: (value) {},
              // onChanged: (String value) {
              //   setState(() {
              //     passssValue = value;
              //   });
              // },
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                  labelText: 'Passss',
                  
                  labelStyle: TextStyle(
                    color: Colors.blue,
                  ),
                  border: OutlineInputBorder()),
              obscureText: true,
              // obscuringCharacter: '',
              maxLength: 20,
            ),
            InkWell(
              onTap: () {
                setState(() {
                  usernameValue = usernameController.text
                  passssValue = passssController.text
                });
              },
              child: Container(
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: 32,
                ),
                child: Container(
                  width: 150,
                  height: 30,
                  color: Color.fromARGB(255, 134, 240, 240),
                  child: Center(
                    child: Text(
                      'Đăng nhập',
                      style:
                          TextStyle(color: Color.fromARGB(255, 241, 102, 21)),
                    ),
                  ),
                ),
              ),
            ),
            Text(
               'username' + $usernameValue ,
              style: TextStyle(fontSize: 30),
            ),
            Text(
              'passss' + $passssValue,
              style: TextStyle(fontSize: 30),
            ),
            
          ],
        ),
      ),
    );
  }
}
