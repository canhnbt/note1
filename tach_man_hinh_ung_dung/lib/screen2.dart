import 'package:flutter/material.dart';

class Screen2 extends StatelessWidget {
  const Screen2({super.key, this.args});
  final ScreenArgument? args;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color.fromARGB(255, 146, 241, 150),
        child: Center(
          child: Column(
            children: [
              Text("Screen 2"),
              Text('CHÀO MỪNG '),
              Text(
                args?.username ?? '',
                style: TextStyle(
                  fontSize: 40,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.popUntil(
                    context,
                    (route) {
                      return ModalRoute.withName("/").call(route);
                    },
                  );
                },
                child: Text("Back lai man 1"),
              )

              //Text(ElevateButton)
            ],
          ),
        ),
      ),
    );
  }
}

class ScreenArgument {
  final String username;
  final String password;
  ScreenArgument({
    required this.username,
    required this.password,
  });
}
