import 'package:flutter/material.dart';
import 'package:man_dang_nhap_data/screen2.dart';
import './screen2.dart';

// You have to add this manually, for some reason it cannot be added automatically

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/",
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case "/":
            return MaterialPageRoute(
              builder: (context) => const MyHomePage(
                title: "On Generated Route",
              ),
              settings: settings,
            );
          case "/screen2":
            return MaterialPageRoute(
              builder: (context) => Screen2(
                args: settings.arguments as ScreenArgument,
              ),
              settings: settings,
            );
        }
        return null;
      },

      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: const MyHomePage(title: 'Stack square'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? usernameValue;
  String? passwordvalue;
  final TextEditingController UsreNameController = TextEditingController();
  final TextEditingController PassWordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.grey.withOpacity(0.2),
        appBar: AppBar(
          title: const Center(child: Text('Messenger')),
          backgroundColor: Colors.purple,
        ),
        body: Container(
          color: Colors.grey,
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Screen 1"),
              Container(
                margin: const EdgeInsets.only(top: 100),
                child: const Center(
                  child: Text(
                    "Flutter",
                    style: TextStyle(
                      fontFamily: "SVNAvo",
                      fontSize: 50,
                      fontStyle: FontStyle.italic,
                      // fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/screen2",
                      arguments: ScreenArgument(
                          username: UsreNameController.text,
                          password: PassWordController.text));
                },
                child: const Text("Chuyen man"),
              ),
              TextField(
                controller: UsreNameController,
                onChanged: (String value) {
                  setState(() {
                    usernameValue = UsreNameController.text;
                  });
                },
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    labelText: 'usre Name',
                    labelStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder()),
              ),
              TextField(
                controller: PassWordController,
                onChanged: (String value) {
                  setState(() {
                    usernameValue = PassWordController.text;
                  });
                },
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                    labelText: 'PassWord',
                    labelStyle: TextStyle(color: Colors.blue),
                    border: OutlineInputBorder()),
                obscureText: true,
              ),
            ],
          ),
        ));
  }
}
