import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Bai Tap'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  width: 400,
                  height: 500,
                  color: Color.fromARGB(255, 243, 239, 239),
                ),
                Positioned(
                  left: 30,
                  top: 40,
                  child: Container(
                    alignment: Alignment.center,
                    child: Text(
                      'intermediate',
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 56, 116, 219)),
                    ),
                    height: 32,
                    width: 120,
                    color: Color.fromARGB(255, 148, 178, 239),
                  ),
                ),
                Positioned(
                    top: 150,
                    left: 30,
                    child: Container(
                      child: Text(
                        'Today\'s \nchallenge ',
                        style: TextStyle(
                            fontSize: 43, fontWeight: FontWeight.w200),
                      ),
                      width: 200,
                      height: 100,
                    )),
                Positioned(
                    left: 30,
                    bottom: 200,
                    child: Container(
                      child: Text(
                        'German meals',
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Color.fromARGB(255, 98, 171, 232)),
                      ),
                      width: 250,
                      height: 36,
                    )),
                Positioned(
                    left: 60,
                    bottom: 135,
                    child: Container(
                      child: Text(
                        'Take this lesson to \nearn a new milestone ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )),
                Positioned(
                    left: 30,
                    bottom: 135,
                    child: Icon(
                      Icons.diamond,
                      size: 30,
                      color: Colors.red,
                    )),
                
              ],
            ),
          ],
        ),
      ),
    );
  }
}
