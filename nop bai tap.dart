import 'dart:developer';

void main() {
  String a = 'dart basic';
  print("Đây là kết quả buổi học thứ 2 về dart: ${a.toUpperCase()} (phần 1)");
  List arr = [
    1,
    2,
    3,
    "đây",
    "kết",
    "là",
    true,
    false,
    {true: "buổi", 1: "học", 10.2: ":", false: "dart basics"},
    ['thứ', 'quả', 'về'],
    "(phần 1)",
    {"flutter": "dart"},
  ];
  print("${arr[3]} ${arr[5]} ${arr[4]} ${arr[9][1]} ${arr[8][true]} ${arr[8][1]} ${arr[9][0]} ${arr[1]} ${arr[9][2]}" +
      " ${arr[11]["flutter"]}${arr[8][10.2]} ${arr[8][false].toString().toUpperCase()} ${arr[10]}");
}
