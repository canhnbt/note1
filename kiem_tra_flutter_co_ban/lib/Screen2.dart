import 'package:flutter/material.dart';

class Screen2 extends StatefulWidget {
  Screen2({super.key, this.args, required RouteSettings settings});
  final ScreenArgument? args;

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  TextEditingController CardNoController = TextEditingController();

  TextEditingController FullNameController = TextEditingController();

  String card1 = '';

  String full1 = '';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Danh Sách thẻ')),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.args?.username ?? '',
              style: TextStyle(
                fontSize: 40,
              ),
            ),
            Stack(
              children: [
                Image.asset(
                  'assets/images/card.png',
                  // width: MediaQuery.of(context).size.width,
                  width: double.infinity,
                ),
                Positioned(
                    bottom: 25,
                    left: 25,
                    child: Column(
                      children: [
                        Text(
                          card1,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 34,
                          ),
                        ),
                        Text(
                          full1,
                          style: TextStyle(color: Colors.white, fontSize: 24),
                        ),
                      ],
                    )),
              ],
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: TextField(
                controller: CardNoController,
                decoration: InputDecoration(labelText: 'Card No'),
                onChanged: (Text) {
                  setState(() {
                    card1 = Text;
                  });
                },
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: TextField(
                onChanged: (Text) {
                  setState(() {
                    full1 = Text;
                  });
                },
                controller: FullNameController,
                decoration: InputDecoration(labelText: 'Full Name'),
              ),
            ),
            Container(
                padding: EdgeInsets.only(top: 15),
                child: ElevatedButton(onPressed: () {}, child: Text('Add'))),
            Container(
              padding: EdgeInsets.only(top: 20),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      'Danh sách thẻ của ${widget.args?.username ?? ""}',
                      style:
                          TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Image.asset(
                "assets/images/card.png",
                width: 250,
                height: 350,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ScreenArgument {
  final String username;
  final String password;
  ScreenArgument({required this.username, required this.password});
}
