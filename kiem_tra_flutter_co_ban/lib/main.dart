import 'package:flutter/material.dart';
import 'package:kiem_tra_flutter_co_ban/Screen2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/':
            return MaterialPageRoute(
              builder: (context) => MyHomePage(title: 'on Generate Route'),
              settings: settings,
            );
          case '/Screen2':
            return MaterialPageRoute(
              builder: (context) => Screen2(
                settings: settings,
                args: settings.arguments as ScreenArgument?,
              ),
            );
        }
      },
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Đăng nhập'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController usernamController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String? UsernameTextError;
  String? PasswordTextError;
  bool isShowPass = false;
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Column(
            children: [
              Image.asset('assets/images/techmaster_black.png',
                  width: double.infinity),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  controller: usernamController,
                  decoration: InputDecoration(
                    labelText: 'Username',
                    errorText: UsernameTextError,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: TextField(
                  controller: passwordController,
                  decoration: InputDecoration(
                      labelText: 'Password',
                      errorText: PasswordTextError,
                      suffixIcon: InkWell(
                        onTap: () {
                          setState(() {
                            isShowPass = !isShowPass;
                          });
                        },
                        child: Icon(isShowPass
                            ? Icons.visibility_off
                            : Icons.visibility),
                      )),
                  obscureText: !isShowPass,
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  if (usernamController.text.isEmpty &&
                      passwordController.text.isEmpty) {
                    setState(() {
                      UsernameTextError = 'khong duoc de trong';
                      PasswordTextError = 'khong duoc de trong';
                    });
                  } else {
                    setState(() {
                      UsernameTextError = null;
                      PasswordTextError = null;
                    });
                    if (usernamController.text.toLowerCase() ==
                            'nguyenvancanh' &&
                        passwordController.text == '123456') {
                      Navigator.pushNamed(context, '/Screen2',
                          arguments: ScreenArgument(
                              username: usernamController.text,
                              password: passwordController.text));
                    }
                  }
                  if (usernamController.text.contains(' ')) {
                    setState(() {
                      UsernameTextError = 'khong duoc chua khoang cach';
                    });
                  }
                },
                child: Text('Đăng Nhập'),
              ),
            ],
          ),
        ));
  }
}
